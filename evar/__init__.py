""" EVAR – Einfach vorlesen auf'm Reader """

from pathlib import Path
from subprocess import check_call
from tempfile import TemporaryDirectory
from typing import Dict
from urllib.parse import quote, urljoin, urlsplit
from urllib.request import urlretrieve

import typer
import yaml  # type: ignore
from requests_html import HTML, Element, HTMLSession

__version__ = '0.1.0'

REGAL_URL = 'https://www.einfachvorlesen.de/geschichtenregal'

app = typer.Typer()


def schreibe_geschichte(dateipfad: Path, geschichte: Element):
    with dateipfad.open('w') as datei:
        for element in geschichte.find('h1,.bookmarkable,img.float'):
            if element.attrs.get('data-index') == '1':
                # Der erste Abschnitt enthält nur Autor, Illustrator, und Verlag im Fließtext
                continue
            elif 'headline' in element.attrs['class']:
                datei.write(f'<p><strong>{element.text}</strong></p>')
            else:
                datei.write(element.html)


def schreibe_metadaten(dateipfad: Path, info: Dict[str, str]):
    with dateipfad.open('w') as datei:
        yaml.safe_dump({
            'title': [{
                'type': 'main',
                'text': info['Titel'],
            }, {
                'type': 'subtitle',
                'text': f'Eine Geschichte für Kinder {info["Alterseinstufung"]}',
            }],
            'creator': [{
                'role': 'author',
                'text': info['Autor/in'],
            }, {
                'role': 'illustrator',
                'text': info['Illustrationen'],
            }],
            'publisher': info['Verlag'],
            'identifier': {
                'scheme': 'ISBN-13',
                'text': info['ISBN'],
            },
        }, datei)


def get_info(geschichte):
    info = {
        'Titel': geschichte.find('.geschichte > h1')[0].text
    }
    for zeile in geschichte.find('.i_center_body')[0].full_text.splitlines():
        if zeile.strip():
            schluessel, wert = zeile.split(':', maxsplit=1)
            info[schluessel] = wert.strip()

    return info


@app.command()
def main(
    zielverzeichnis: Path = typer.Argument(Path('.')),
    epub: bool = typer.Option(True, help='Speichere die Geschichten als EPUB-Dateien.'),
    pdf: bool = typer.Option(True, help='Speichere die Geschichten als PDF-Dateien.'),
):
    session = HTMLSession()
    regal = session.get(REGAL_URL)
    regal.raise_for_status()

    altersgruppen = {altersgruppe.attrs['data-altersgruppe'] for altersgruppe in regal.html.find('div.alter')}

    geschichtenseiten = {}
    for altersgruppe in altersgruppen:
        regal_fuer_altersgruppe = session.post(REGAL_URL, data={
            'alter': altersgruppe
        }, headers={
            'X-OCTOBER-REQUEST-HANDLER': 'storiesListRegal::onToggleView',
            'X-OCTOBER-REQUEST-PARTIALS': '@story-list-container',
            'X-Requested-With': 'XMLHttpRequest',
        })
        regal_fuer_altersgruppe.raise_for_status()
        base_url = regal_fuer_altersgruppe.url

        regal_fuer_altersgruppe = HTML(html=regal_fuer_altersgruppe.json()['@story-list-container'])
        felder = regal_fuer_altersgruppe.find('div.story-item')

        geschichtenseiten.update({
            feld.find('a.lesen_button')[0].attrs['href']: urljoin(
                base_url, feld.find('.story_cover > img')[0].attrs['src']
            ) for feld in felder
        })

    for geschichten_url, titelbild_url in geschichtenseiten.items():
        slug = urlsplit(geschichten_url).path.split('/')[-1]
        typer.echo(f"Slug: {slug} -> {titelbild_url}")
        pdf_file = zielverzeichnis / f'{slug}.pdf'
        epub_file = zielverzeichnis / f'{slug}.epub'

        if not pdf_file.exists() or not epub_file.exists():
            geschichte = session.get(geschichten_url)
            geschichte.raise_for_status()

            if pdf and not pdf_file.exists():
                urlretrieve(geschichte.html.find('.piwik_download')[0].attrs['href'], filename=pdf_file)

            if epub and not epub_file.exists():
                with TemporaryDirectory() as temp_dir:
                    temp_dir_path = Path(temp_dir)

                    info = get_info(geschichte.html)
                    info_datei = temp_dir_path / 'meta.yml'
                    schreibe_metadaten(info_datei, info)

                    inhalt = geschichte.html.find('.geschichte')[0]

                    titelbild = temp_dir_path / 'titelbild.jpg'
                    urlretrieve(quote(titelbild_url, safe=':/'), titelbild)

                    inhalt_als_datei = temp_dir_path / 'geschichte.html'
                    schreibe_geschichte(inhalt_als_datei, inhalt)

                    check_call([
                        'pandoc',
                        inhalt_als_datei,
                        '--output', epub_file,
                        f'--metadata-file={info_datei}',
                        f'--epub-cover-image={titelbild}',
                        '--to=epub2',  # Tolino claims the epub3 generated by default is broken.
                    ])
