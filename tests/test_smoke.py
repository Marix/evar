import os
from pathlib import Path

from typer.testing import CliRunner

from evar import app

runner = CliRunner()


def test_defaults(tmp_path: Path):
    os.chdir(tmp_path)

    result = runner.invoke(app)
    assert result.exit_code == 0

    # Es gibt je vier Geschichten für drei Altersgruppen
    assert len(list(tmp_path.glob('*.pdf'))) >= 12
    assert len(list(tmp_path.glob('*.epub'))) >= 12


def test_no_pdf(tmp_path: Path):
    result = runner.invoke(app, [str(tmp_path), '--no-pdf'])
    assert result.exit_code == 0

    # Es gibt je vier Geschichten für drei Altersgruppen
    assert len(list(tmp_path.glob('*.pdf'))) == 0
    assert len(list(tmp_path.glob('*.epub'))) >= 12


def test_no_epub(tmp_path: Path):
    result = runner.invoke(app, [str(tmp_path), '--no-epub'])
    assert result.exit_code == 0

    # Es gibt je vier Geschichten für drei Altersgruppen
    assert len(list(tmp_path.glob('*.pdf'))) >= 12
    assert len(list(tmp_path.glob('*.epub'))) == 0
