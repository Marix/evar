from pathlib import Path

import yaml  # type: ignore
from requests_html import HTML

from evar import get_info, schreibe_metadaten


def test_get_info():
    geschichte = HTML(html=Path('tests/beispiel.html').read_text())
    info = get_info(geschichte)

    assert info['Titel'] == 'Die Geschichte'
    assert info['Autor/in'] == 'Autorin Musterfrau'
    assert info['Illustrationen'] == 'Illustrator Musterman'
    assert info['Verlag'] == 'Beispielverlag'
    assert info['ISBN'] == '123-4-567-8901-2'
    assert info['Alterseinstufung'] == 'ab 7 Jahren'


def test_schreibe_metadaten(tmp_path):
    info = {
        'Titel': 'Ein Titel',
        'Autor/in': 'Eine Autorin',
        'Illustrationen': 'Ein Illustrator',
        'Verlag': 'Ein Verlag',
        'ISBN': '123-4-567-8901-2',
        'Alterseinstufung': 'ab 7 Jahren',
    }

    info_pfad = tmp_path / 'meta.yml'
    schreibe_metadaten(info_pfad, info)

    written = yaml.safe_load(info_pfad.open())

    assert written['title'] == [
        {'type': 'main', 'text': 'Ein Titel'},
        {'type': 'subtitle', 'text': 'Eine Geschichte für Kinder ab 7 Jahren'},
    ]
    assert written['creator'] == [
        {'role': 'author', 'text': 'Eine Autorin'},
        {'role': 'illustrator', 'text': 'Ein Illustrator'},
    ]
    assert written['identifier'] == {
        'scheme': 'ISBN-13',
        'text': '123-4-567-8901-2',
    }
    assert written['publisher'] == 'Ein Verlag'
