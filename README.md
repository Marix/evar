EVAR – Einfach vorlesen auf'm Reader
====================================

Evar erlaubt es die Geschichten von <https://www.einfachvorlesen.de/> ins Epub-Format zu wandeln um sie anschließen auf einem Ebook Reader vorlesen zu können.

Nutzung
-------

Einfach `evar` oder `python3 -m evar` ausführen. Die aktuellen Geschichten werden als PDF und Epub im aktuellen Verzeichnis gespeichert und können dann auf den Reader kopiert werden.

Systemvorraussetzungen
----------------------

Das Skript benötigt [Python3](https://www.python.org/) und [Pandoc](https://pandoc.org/).
Alle benötigten Python-Biblotheken sind in den Metadaten deklariert und werden bei der Installation automatisch mitinstalliert.

Installation
------------

`pip3 install evar`

Lizens
------

Evar ist freie Software und darf unter den Bedingungen der [GPL3](COPYING) genutzt, verändert und weitergegeben werden.
