Versionshistorie
================

Hier sind die wichtigsten Änderungen in den einzelnen Versionen von Evar aufgeführt.

Unveröffentlicht
----------------

* Das Verzeichnis, in dem die Geschichten abgelegt werden sollen, kann als Parameter übergeben werden.
* Es können optional nur die PDFs oder nur die EPUB-Dateien geladen werden.
* Neue Abhängigkeit: `typer`.

Version 0.1.0
-------------

* Die Geschichten von <https://www.einfachvorlesen.de/> werden im aktuellen Verzeichnis als PDF und Epub gespeichert.
